﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace OneWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Event> Get()
        {
            return new List<Event>
            {
                new Event { Name = "Christmas", Description = "Happy day" },
                new Event { Name = "Carnival", Description = "Happy day" }
            };
        }
    }

    public class Event
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
