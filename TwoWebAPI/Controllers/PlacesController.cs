﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace OneWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlacesController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Place> Get()
        {
            return new List<Place>
            {
                new Place { Name = "everywhere" },
                new Place { Name = "house" },
                new Place { Name = "city" }
            };
        }
    }

    public class Place
    {
        public string Name { get; set; }
    }
}
